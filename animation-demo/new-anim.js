var block = document.querySelector(".block");
var button = document.querySelector("#newGame");
var startBlock = document.querySelector(".start-block");
var cellFirst = document.querySelector("#cell1");
var cellForth = document.querySelector("#cell4");
var xItem = document.getElementById("cross");
var oItem = document.getElementById("null");


button.addEventListener("click",function(){
    playGame();
});

function playGame() {
    xItem.style.display = "none";
    oItem.style.display = "none";
}


cellFirst.addEventListener("click", function () {
    displayX();
});
cellForth.addEventListener("click", function () {
    displayO();
});

function displayX(){
    xItem.style.display = "block";
    xItem.style.margin = "-290px 0 0 20px"
}

function displayO() {
    oItem.style.display = "block";
    oItem.style.margin = "-190px 20px";
}